#include "neutrino.h"

using namespace std;

int main() {

	gSystem->Load("libTree.so");
	fstream file;
	file.open ("files.txt", ios::in);
	
	cout << "begin run " << endl;
	
	if(file.is_open())
		cout << "Abrio bien: " << "file.txt"  << endl;
	if(file.fail()) 
		cout << "problemas con el archivo" << endl;

	vector<TH1D> hist; 
	vector<TGraphErrors> exp;  

	vector<TGraph> tg;  
	vector<TGraph> tgl; 

	hist.reserve(10);
	TH1D untest("ENeutronall", " ;Energy (MeV); Probability", 600, 0, 1200);
	cout << hist.size() << endl;
	hist.push_back(untest);  		

	vector<TString> fnames;
	fnames.reserve(20);

	vector<double> events;
	events.reserve(8);

	vector<double> events_like;
	events_like.reserve(8);

		
		while(true) {      
		if (!file.good() ) break; 
		string pdline, Stemp;
		getline(file, pdline);   
		istringstream istr(pdline.c_str());      
		istr >> Stemp;
		if(Stemp == "" )
		break;
		fnames.push_back(Stemp);
				
		} 
	
	file.close();
	cout << fnames.size() << endl;
	
	TString nuc;
	TString nuctem;
	TString en;
	find_data(fnames[0],nuc, en);
	cout << nuc << endl;
	cout << en << endl;	


	//create_hist_exp(hist,exp,find_data(fnames[0]));

	create_tgs(tg,tgl);
			
        int mycounter = 0;
	for (auto it = fnames.begin() ; it != fnames.end(); ++it){
		analisis(*it, events, events_like);
		cout << endl;
		mycounter++;
      		find_data(*it,nuc, en);
		cout << nuc << " " << en << endl; 
		setgr(mycounter,en,events,tg);
		setgr(mycounter,en,events_like,tgl);   
		for(int i=0; i<8; i++ ){
//			cout << events[i] << " ";	
			events[i]=0;
			events_like[i] = 0;	
		}	
	}


/*	for(int i=0; i<8; i++ ){
		cout << events[i] << " ";	
		events[i]=0;
		events_like[i] = 0;	
		}
	cout << endl;
*/
//	save_hist(hist);

	TString name = "_cim.pdf";

	array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};	 
	cout << reaction[2] << endl;
	
/*	TMultiGraph *mp_exp = new TMultiGraph();
       TGraph *CCqe1e = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCqe1e.txt","%lg %lg %lg"); CCqe1e->SetLineColor(kBlue);
       TGraph *CCqe2e = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCqe2e.txt","%lg %lg %lg"); CCqe2e->SetLineColor(kRed);
       TGraph *CCqe3e = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCqe3e.txt","%lg %lg %lg"); CCqe3e->SetLineColor(kGreen);
       TGraph *CCqe4e = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCqe4e.txt","%lg %lg %lg"); CCqe4e->SetLineColor(kOrange);
       TGraph *CCqe5e = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCqe5e.txt","%lg %lg %lg"); CCqe5e->SetLineColor(kBlack);
       
    TMultiGraph *mppp_exp = new TMultiGraph();
       TGraph *CCres1e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres1e.txt","%lg %lg %lg"); CCres1e->SetLineColor(kBlue);
       TGraph *CCres2e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres2e.txt","%lg %lg %lg"); CCres2e->SetLineColor(kRed);
       TGraph *CCres3e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres3e.txt","%lg %lg %lg"); CCres3e->SetLineColor(kGreen);

	TMultiGraph *mppn_exp = new TMultiGraph();
       TGraph *CCres4e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres4e.txt","%lg %lg %lg"); CCres4e->SetLineColor(kBlue);
       TGraph *CCres5e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres5e.txt","%lg %lg %lg"); CCres5e->SetLineColor(kRed);
     
	TMultiGraph *mpop_exp = new TMultiGraph();
       TGraph *CCres6e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres6e.txt","%lg %lg %lg"); CCres6e->SetLineColor(kBlue);
       TGraph *CCres7e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/CCres7e.txt","%lg %lg %lg"); CCres7e->SetLineColor(kRed);
    
    TMultiGraph *nuppn_exp = new TMultiGraph();
       TGraph *NCres1e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres1e.txt","%lg %lg %lg"); NCres1e->SetLineColor(kBlue);
   
    TMultiGraph *nupop_exp = new TMultiGraph();
       TGraph *NCres2e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres2e.txt","%lg %lg %lg"); NCres2e->SetLineColor(kBlue);
       TGraph *NCres3e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres3e.txt","%lg %lg %lg"); NCres3e->SetLineColor(kRed);
   
    TMultiGraph *nupon_exp = new TMultiGraph();
       TGraph *NCres4e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres4e.txt","%lg %lg %lg"); NCres4e->SetLineColor(kBlue);
    
    TMultiGraph *nupmp_exp = new TMultiGraph();
       TGraph *NCres5e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres5e.txt","%lg %lg %lg"); NCres5e->SetLineColor(kBlue);
       TGraph *NCres6e.txt = new TGraph("~/home/danaisis/Documents/Maestria/Dato_ajustes/exp/NCres6e.txt","%lg %lg %lg"); NCres6e->SetLineColor(kRed);

*/
	for(int i=0; i<8; i++ ){	
		TCanvas *c1 = new TCanvas("c1","Total cross section",800,600);
		
        c1->cd();                                              
        c1->SetLogx();
		tg[i].GetXaxis()->SetTitle("E_{#nu} (MeV)");
		tg[i].GetXaxis()-> CenterTitle();
		tg[i].GetYaxis()-> CenterTitle();
		tg[i].SetLineColor(kOrange);
        tgl[i].SetLineColor(kRed);
		tg[i].GetXaxis()->SetRangeUser(100, 1600);
		tg[i].GetYaxis()->SetTitle("#sigma/nucleon (*10^{-8} cm^{2}/nucleon)");
		//the graphics
		tg[i].Draw("AP");
		tgl[i].Draw("SAME");
		//archive name
//		c1->Print(reaction[i]+name);
        c1->Print("Total cross section"+reaction[i]+name);
        
        //legend
//      leg = new TLegend(0.1,0.7,0.48,0.9);
//      leg->SetHeader("The Legend Title");
//      leg->AddEntry(tg[i],reaction[i]+"-true","t");
//      leg->AddEntry(tgl[i],reaction[i]+"-like","l");
//      leg->Draw();

	delete c1;
	
	}

/*	for(int i=0; i<8; i++ ){	
		TCanvas *c2 = new TCanvas("c2","Total cross section",800,600);                                           		
		c2->cd();                                               
		c2->SetLogx();
		tgl[i].GetXaxis()->SetTitle("E_{#nu} (MeV)");
		tgl[i].GetXaxis()-> CenterTitle();
		tgl[i].GetYaxis()-> CenterTitle();
		tgl[i].GetXaxis()->SetRangeUser(100, 1600);
		tgl[i].GetYaxis()->SetTitle("#sigma/nucleon (*10^{-8} cm^{2}/nucleon)");
		tgl[i].Draw("AP ");
    	c2->Print(reaction[i]+"like"+name);
    	
	delete c2;
	
	}*/


	return 0;
}



bool create_tgs(vector<TGraph> &tg, vector<TGraph> &tgl){

        array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};
	TString hnameFormat = "_like" ;
	
	TGraph temp;
	TGraph temp1;
	int n = 15;	

  	for ( auto it = reaction.begin(); it != reaction.end(); ++it ){
		//		cout << hname << " " << endl;
		temp1 = TGraph(15);
		temp1.SetTitle(*it);
		temp1.SetMarkerStyle(21);
		temp1.SetMarkerSize(0.8);
/*		temp1.GetXaxis()->SetTitle("Energy");
		temp1.GetXaxis()->CenterTitle();
		temp1.GetYaxis()->SetTitle("Probability");
		temp1.GetYaxis()->CenterTitle();
*/		tg.push_back(temp1);
		temp1.SetTitle(*it+hnameFormat);				
		tgl.push_back(temp1);	
		}



 return 0;
}

bool create_hist(vector<TH1D> &h){
	/*

	TString r1 = "";
	TString r2 = "";
	TString r3 = "";
	TString r4 = "";
	TString r5 = "";
	TString r6 = "";
	TString r7 = "";


	 array< TString,13 > reaction ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};
	 array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};


	
	  TString hnameFormat = " " ;
       	  TString hname;

	  TString hnameFormat1 = "_like" ;
       	  TString hname1; 	


 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;


	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}*/
	return true;
}


bool create_hist_exp(vector<TH1D> &h, vector<TGraphErrors> &exp, TString format){
	
	/* array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};

	  array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};		

	  TString hnameFormat = "Ejected Neutrons at " ;
       	  TString hname; 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;

	
	TString nametest;		        
	 cout << " test   " << angle.at(0) << endl;;

	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
		nametest =  format+"ddxs_"+*it+"_leray.txt";
		cout << nametest  << endl;		
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}*/
	return true;
}

bool analisis(TString tname, vector<double> &ev,  vector<double> &evl){
	
	TFile* f = NULL;

	cout << tname << endl;
		
	TBranch *TBPart_NPart, *TBPart_Mes_PID, *TBPart_E, *TBPart_PX, *TBPart_PY, *TBPart_PZ, *TBPart_K;	
	TBranch *TBNuc_A, *TBNuc_Z, *TBNuc_Energy, *TBNuc_phoCnt, *TBPart_PID, *TBinitialAttempts;
	
	
	
	f = new TFile(tname);
	


		
	
	if (f->IsZombie()) {
		cout << "Error opening file " << tname << endl;
		delete f;
		return false;
	} 
	else{
		cout << tname << " file sucessfully openend" << endl;
	//	f->ls();
		
		TTree *t2 = (TTree*) f->Get("History");
		
		Double_t cascatas_efetivas = 0;                             /// effective cascades counter
        Double_t neventos = 0;                                      /// number of attempts counter for the effective cascades
        Double_t An = 12;                                           /// masic number of the targer
        Double_t rnt = 1.18 * pow(double(An),1./3.);                /// Nuclear radius 
        Double_t sf = TMath::Pi() * pow((rnt),2.);                  /// fm^2 -> microbarn 
                                        
        
		Int_t A; Int_t Z; 
		Double_t nucEnergy; 
		Int_t phoCnt; Int_t NPart; 
		Int_t Part_PID[200]; 
		Double_t Part_PX[200];
		Double_t Part_PY[200]; 
		Double_t Part_PZ[200]; 
		Double_t Part_E[200];
		Double_t Part_K[200]; 
		Int_t initialAttempts=0;
		
		TBNuc_A = t2->GetBranch("finalNuc_A");
		TBNuc_A->SetAddress(&A);
		TBNuc_Z = t2->GetBranch("finalNuc_Z");
		TBNuc_Z->SetAddress(&Z);
		TBNuc_Energy = t2->GetBranch("finalNuc_Ex");
		TBNuc_Energy->SetAddress(&nucEnergy);
		TBPart_NPart = t2->GetBranch("UnBindPart_Number");
		TBPart_NPart->SetAddress(&NPart);
		TBPart_PID = t2->GetBranch("UnBindPart_Pid");
		TBPart_PID->SetAddress(&Part_PID[0]);
		TBPart_PX = t2->GetBranch("UnBindPart_PX");
		TBPart_PX->SetAddress(&Part_PX[0]);
		TBPart_PY = t2->GetBranch("UnBindPart_PY");
		TBPart_PY->SetAddress(&Part_PY[0]);
		TBPart_PZ = t2->GetBranch("UnBindPart_PZ");
		TBPart_PZ->SetAddress(&Part_PZ[0]);
		TBPart_K = t2->GetBranch("UnBindPart_K");
		TBPart_K->SetAddress(&Part_K[0]);
		TBPart_E = t2->GetBranch("UnBindPart_E");
		TBPart_E->SetAddress(&Part_E[0]);
		t2->SetBranchAddress("initialAttempts",&initialAttempts);
		

				int dmcounter=0;

		std::vector<int> conteos;
		conteos.reserve(8);

		for (int k=0; k<8; k++){
			conteos[k]=0;
			ev[k]=0.;
			evl[k]=0.;	
		}
	//	cout << conteos.size() << endl;
		int cn = 0,cp = 0,cm=0 ,cne = 0,cpm = 0,cp0 = 0,cpp = 0;		
        
		Int_t nentries = (Int_t)t2->GetEntries();
		cout << nentries << endl;

		int false_counter = 0;		
		for (Int_t i=0; i<nentries; i++) { // 
		t2->GetEntry(i);
			
			if(nucEnergy<0.0)
				{
					false_counter++;
					continue;	
				}
			else{
				
				 neventos += initialAttempts;    // Number of attempts to get an effective cascade
				 cascatas_efetivas = cascatas_efetivas+1.; // Count of the attempts and effective cascades
				
				for (int k=0; k<7; k++){
				conteos.push_back(0);
				}

				for (Int_t j=0; j < NPart; j++) {
				// aqui va el analisis de las partículas
		
					if(Part_PID[j]==2112)
						conteos[0]+=1; //neutrones

					if(Part_PID[j]==2212)
						conteos[1]+=1; //protones

					if(Part_PID[j]==13)
						conteos[2]+=1; //muones

					if(Part_PID[j]==211)
						conteos[3]+=1; // pi mas


					if(Part_PID[j]==111)
						conteos[4]+=1; // pi 0

					if(Part_PID[j]==-211)
						conteos[5]+=1; // pi menos

					if(Part_PID[j]==14)
						conteos[6]+=1; //neutrino
					

					if(Part_PID[j]==2214)
						dmcounter++;
					}
					
							
					if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==0& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[0]+=1.; //mp 

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==1& conteos[3]==0& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[0]+=1.; //mp like

					if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[1]+=1.; // mppp

					if(conteos[0]==0 & conteos[1]>=0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[1]+=1.; // mppp like 

                    if(conteos[0]==1 & conteos[1]==0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[2]+=1.; // mppn

					if(conteos[0]>=0 & conteos[1]==0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[2]+=1.; // mppn like 

                    if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==0)
						ev[3]+=1.; // mpop

					if(conteos[0]>=0 & conteos[1]>=0 & conteos[2]==1& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==0)
						evl[3]+=1.; // mpop like 
                                     
                    if(conteos[0]==1 & conteos[1]==0& conteos[2]==0& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==1)
						ev[4]+=1.; // nuppn

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==0& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==1)
						evl[4]+=1.; // nuppn like 

                    if(conteos[0]==0 & conteos[1]==1& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						ev[5]+=1.; // nupop

					if(conteos[0]==0 & conteos[1]>=0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						evl[5]+=1.; // nupop like 
					
					if(conteos[0]==1 & conteos[1]==0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						ev[6]+=1.; // nupon

					if(conteos[0]>=0 & conteos[1]==0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						evl[6]+=1.; // nupon like 

                    if(conteos[0]==0 & conteos[1]==1& conteos[2]==0& conteos[3]==0& conteos[4]==0& conteos[5]==1& conteos[6]==1)
						ev[7]+=1.; // nupmp

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==0& conteos[3]==0& conteos[4]==0& conteos[5]==1& conteos[6]==1)
						evl[7]+=1.; // nupmp like 



/*
					if(conteos[0] & conteos[1]& conteos[2]& conteos[3]& conteos[4]& conteos[5]& conteos[6])	

					if(conteos[2]!=0 & (conteos[3]!=0 & (conteos[0]!=0 & conteos[1]==0)))
						ev[2]=ev[2]+1.;	// mppp
					if(conteos[2]!=0 & conteos[3]!=0 & conteos[0]!=0 & conteos[1]!=0 )
						ev[3]=ev[3]+1.;	// 				
					if(conteos[2]==0 & conteos[3]!=0 & conteos[0]!=0 & conteos[1]!=0 )
						ev[4]=ev[4]+1.;					
*/

					for (int k=0; k<7; k++){
					conteos[k]=0;
	//				cout << conteos[k] << endl;
					}


	//			if(i<10)
	//	cout << cn << " " << cp << " " << cm << " " << cp0 << " " << cpp << " " << cpm << " " << cne <<  endl;

				}	
	
			}
		
		

		cout << "deltas que no deacaen " <<  dmcounter << endl; 
		dmcounter = 0;
/*		cout <<ev[0] << " " << ev[1] << " " << ev[2] << " " << ev[3] << " " << ev[4] << " " << ev[5];
		cout << " " << ev[6] <<  endl;
		cout << false_counter << endl;
		cout << false_counter+cm << endl;
		cout << "mppn " << ev[2] << " eventos cn proton ademas " << ev[3] << endl;
		cout << "sale m p+ p " << ev[4] << endl << endl; 
*/

		double peso[8] = {0.012,0.025,0.025,0.04,0.03,0.037,0.002,0.004};
		double nucleon[8] = {12,6,6,6,6,6,6,6};
		
	//	double factor =  sf / (double(nentries - false_counter));
        double factor =  sf / (double(neventos));
        
		cout << " factor " << factor << endl;
		
		for (int k = 0; k<8; k++){
		//cross section total	
			cout << ev[k];
			ev[k] =(ev[k]*factor)/(peso[k]*nucleon[k]);
			cout << " "<< ev[k] << endl ;
		//	cout << ev[k] << endl;
			evl[k] =(evl[k]*factor)/(peso[k]*nucleon[k]);
			
		//multiplicity	
			
	}

		delete t2;
		delete f;
	

	
	return true;
	}
	
	
	
}



Double_t theta_calc(Double_t px, Double_t py, Double_t pz){
/*
	TVector3 vec_x (1.,0.,0.);
 	TVector3 vec_P(px,py,pz);
 	
 	Double_t theta = vec_P.Angle(vec_x)*(180.)/(TMath::Pi());
 	
	return theta;*/
	return 1.;

} // Cierro P_calc



void set_h(double ang, std::vector<TH1D>& h, double k){ /*
	h[0].Fill(k);
	
	if( ang>7 && ang<12 )
		h[1].Fill(k);	
	else if ( ang>22 && ang<26 )
		h[2].Fill(k);
	else if ( ang>27 && ang<32 )
		h[3].Fill(k);
	else if ( ang>37 && ang<42 )
		h[4].Fill(k);
	else if ( ang>57 && ang<62 )
		h[5].Fill(k);
	else if ( ang>82 && ang<87 )
		h[6].Fill(k);
	else if ( ang>97 && ang<102 )
		h[7].Fill(k);
	else if ( ang>112 && ang<117 )
		h[8].Fill(k);
	else if ( ang>117 && ang<122 )
		h[9].Fill(k);
	else if ( ang>127 && ang<132 )
		h[10].Fill(k);
	else if ( ang>142 && ang<146 )
		h[11].Fill(k);
	else if ( ang>147 && ang<152 )
		h[12].Fill(k);
	else if (ang>157 && ang<162)
		h[13].Fill(k);*/


}

void save_hist(std::vector<TH1D>& h){
	/*
	TFile* myfile;
	myfile = new TFile("cascade.root","recreate");

	for(int i=0; i<h.size(); i++){
		h[i].Write();	
		
	}
	myfile->Close();
	delete myfile;*/

}


void find_data(TString name, TString &nuc, TString &en){
	   int n1 = name.Last('/')+1;
	   int n2 = name.Last('M');
	   int n3 = name.First('_');
  	  nuc = TString(name(n1,n3-n1));	
	  en  = TString(name(n3+1,n2-n3-1));			
	
}


bool setgr(int idx, TString E, std::vector<double>& vals,  vector<TGraph> &grs){

	for (int i=0; i<8; i++ ) {	
		grs[i].SetPoint(idx,E.Atof(),vals[i]);
//		cout << vals[i] << endl;
	}

	return true;

}


