#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <string>



#include "DataHelper.hh"

#include "TROOT.h"
#include "TBrowser.h"
#include "Riostream.h"
#include "TH1.h"
#include "TFile.h"
#include "TRandom.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TList.h"
#include "TObjArray.h"
#include "TChain.h"
#include "TSystem.h"
#include "TMath.h"
#include "TVector3.h"
#include "TCanvas.h"
#include "TRegexp.h"
#include "TSpline.h"

bool create_hist(std::vector<TH1D>& h);

bool create_hist_exp(std::vector<TH1D> &h, std::vector<TGraphErrors> &exp, TString format);

bool analisis(TString tname, std::vector<TH1D>& h);

void set_h(double ang, std::vector<TH1D>& h, double k);

Double_t theta_calc(Double_t px, Double_t py, Double_t pz);

void save_hist(std::vector<TH1D>& h);

void load_graphs();

void ps(double E, double m, double &val ); // recive max energy, lepton mass and val


TString find_data(TString name);

double resonance_mass( double em_upper);

double breitWigner( Double_t sqrt_s, Double_t* p );

void plotdata();

