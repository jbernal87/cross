#include "cross.h"


// resonance_mass(em_upper,out_particles[1].PdgId());

using namespace std;

int main() {


	double yval=0;
	double yvalm=0;

	TGraph test =  TGraph(50);
	TGraph BW =  TGraph(15);
	double mu = 105/1000.;
	double m0 = 0.00000000000001;
	double delm =  1232/1000.;
	double neu_E = 0;	
	double mp = 938.3/1000.; 
	double mn = 939.5/1000.;
	double width = 0.118;  
	
	Double_t p33[3];
	p33[0] = delm;
	p33[1] = width;
	p33[2] = 1.;	


	int counter = 0;
	double max_E = 0.;
	double ne = 0;
	for (int i = 100; i<2000; i+=5){
		ne = (double) i/1000.;

		double lc =  0;
		double prom = 0;
		double prom2 = 0;	
		while(lc < 100000.){
			prom += resonance_mass(i); 
			prom2 += resonance_mass(delm*1000);
			lc+=1.;
		}
		double bb = prom/lc;
		double bb1 =prom2/lc;

		cout << bb << " " << bb1 << endl;
		
//		cout << ne + mp <<endl;
//		cout << sqrt( i*i + mp*mp*1000000 )/1000 <<endl;
//		cout << sqrt( ne*ne + mp*mp ) - delm << " aa "  << i << endl;
		max_E =TMath::Sqrt( ne*ne + mp*mp - bb1*bb1/1000000  ) ;
//    		max_E = ne + 0.95*mp - 0.95*bb1/1000 -mu ;
		if(max_E>0){
//		cout << max_E << endl;
		
		ps(max_E,m0,yval);
		ps(max_E,m0,yvalm);
		yval = yval/yvalm;
		}
		else{
			yval = 0;
			continue;	
		}
//		cout << i << "  " << yval/1000. << endl;
		counter++;		

//		double bb = breitWigner( sqrt(ne*ne + mn*mn),p33);
//		double bb1 = breitWigner(delm,p33);

//		double bb = resonance_mass(i);

		

/*		if(sqrt(ne*ne + mn*mn)>delm){
			yval=yval*bb1;
		//	
		}
		else{
			yval=yval* bb ;
			}
*/
	//	double peso =1;
		double peso =0.331;
	//	double peso =0.663;
		yval =  yval*bb/bb1;		
		test.SetPoint(counter,ne,peso*0.4*yval);
	}		
	
	

	
//	nu+p -> delta ++ mu  
/*	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("nu+p -> delta ++ mu  ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/CCres1e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/CCres2e.txt","%lg %lg %lg");
//	TGraphErrors ndata2 = TGraphErrors("~/Dropbox/cross/dppdata2.txt","%lg %lg %lg");
*/

/*	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("nu+n -> delta+ mu pi+  ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/CCres4e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/CCres5e.txt","%lg %lg %lg");
*/

///home/jlbernal/Dropbox/cross/data/NCres1e.txt

        
        // hacer para 2
	plotdata();
	
	TFile *file=new TFile("2.root","new");
	file->cd();
	test.SetMarkerStyle(20);
	test.SetMarkerSize(0.6);
	test.Write();
	delete file;
	
/*
 * activar esta pinga para delta ++ no tocar ni yo mismo
 * mp
 * peso 1
 * 0.7 s0
	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+n -> #Delta^{++} + #mu^{-} -> #mu^{-} + #pi^{+} + p ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/CCres1e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/CCres2e.txt","%lg %lg %lg");
	TGraphErrors ndata2 = TGraphErrors("~/Dropbox/cross/data/CCres3e.txt","%lg %lg %lg");
	
	mg->Add(&ndata);
	mg->Add(&ndata1);
	mg->Add(&ndata2);
*/

/*	mn
 * 	peso 0.661 // ojo aqui
 * 	0.4 S0
 */ 	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+n -> #Delta^{+} + #mu^{-} -> #mu^{-} + #pi^{+} + n ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/CCres4e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/CCres5e.txt","%lg %lg %lg");	
	
	mg->Add(&ndata);
	mg->Add(&ndata1);
	



/*	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+n -> #Delta^{+} + #mu^{-} -> #mu^{-} + #pi^{0} + p ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/CCres6e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/CCres7e.txt","%lg %lg %lg");
	
	mg->Add(&ndata);
	mg->Add(&ndata1);
*/

/* 0.2
 *	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+p -> #Delta^{+} + #nu -> #nu + #pi^{+} + n ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/NCres1e.txt","%lg %lg %lg");
	mg->Add(&ndata);
*
*/


/*	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+p -> #Delta^{+} + #nu -> #nu + #pi^{0} + p ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/NCres2e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/NCres3e.txt","%lg %lg %lg");
	mg->Add(&ndata);
	mg->Add(&ndata1);

*/

/*
 * 0.1 s0
	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+n -> #Delta^{0} + #nu -> #nu + #pi^{0} + n ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/NCres4e.txt","%lg %lg %lg");
	mg->Add(&ndata);
 */


/*	TMultiGraph *mg =  new TMultiGraph();
	mg->SetTitle("#nu+n -> #Delta^{0} + #nu -> #nu + #pi^{-} + p ");
	TGraphErrors ndata = TGraphErrors("~/Dropbox/cross/data/NCres5e.txt","%lg %lg %lg");
	TGraphErrors ndata1 = TGraphErrors("~/Dropbox/cross/data/NCres6e.txt","%lg %lg %lg");
	mg->Add(&ndata);
	mg->Add(&ndata1);
*/

	TCanvas *c1 = new TCanvas("c1","",800,600) ;
	test.GetXaxis()->SetRangeUser(0,4000);

	

	mg->Add(&test);	
	mg->Draw("AP");
	mg->GetXaxis()->SetTitle("Energy (GeV)");
	mg->GetXaxis()->CenterTitle();
	mg->GetYaxis()->SetTitle("Cross Section (10^{-38} cm^{-2})");
	mg->GetYaxis()->CenterTitle();
	mg->GetYaxis()->SetTitleOffset(1.4); 
	mg->GetXaxis()->SetRangeUser(0.,2.);

   gPad->Modified();
//	s3->Draw(" same");

//	c1->SetLogx();
	c1->Print("integral.pdf");
	
	

	delete mg;
	delete c1;
	
	
//	cout << breitWigner(delm,p33) << endl;
	
/*	//X
(0.451011,0.627131,0.891404,1.11051,1.38347,2.01016,4.24379) proton - delta++ + muon
//ERROR X
(0,0,0,0,0,0,0)
//Y
(0.0351562,0.152344,0.371094,0.484375,0.585938,0.777344,0.742188)
//ERROR Y
(0.0215,0.0299,0.0547,0.0918,0.1192,0.16015,0.26365)*/
	


	TCanvas *c2 = new TCanvas("c2","",800,600) ;
	BW.SetMarkerStyle(2);
	BW.SetMarkerSize(1);
	ndata.Draw("AP");
	c2->Print("BW.pdf");
	
	delete c2;
	
	return 0;
}


void ps(double E, double m, double &val ){
	
	double E2 = E*E;
	double m2 = m*m;
	double rest = E2-m2;
	if(rest<=0)
		val = 0;
	else{	
	double P = TMath::Sqrt(rest);
	
	val = E*P + m2*TMath::Log(m/(E+P));
	}
}


bool create_hist(vector<TH1D> &h){
	
	 array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};
	
	  TString hnameFormat = "Ejected Neutrons at " ;
       	  TString hname; 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;


	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}
	return true;
}


bool create_hist_exp(vector<TH1D> &h, vector<TGraphErrors> &exp, TString format){
	
	 array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};
		

	  TString hnameFormat = "Ejected Neutrons at " ;
       	  TString hname; 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;

	
	TString nametest;		        
	 cout << " test   " << angle.at(0) << endl;

	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
		nametest =  format+"ddxs_"+*it+"_leray.txt";
		cout << nametest  << endl;		
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}
	return true;
}

bool analisis(TString tname, vector<TH1D> &h){
	
	
	return true;
}




void save_hist(std::vector<TH1D>& h){

	TFile* myfile;
	myfile = new TFile("cascade.root","recreate");

	for(int i=0; i<h.size(); i++){
		h[i].Write();	
		
	}
	myfile->Close();
	delete myfile;

}


TString find_data(TString name){
	   int n1 = name.Last('/')+1;
	   int n2 = name.Last('M');
           TString sname =  name(n1,n2-n1);	 					
	   return "/home/jlbernal/mandatory_full/neutron/mandatory/data/p_"+sname(0,2)+"_"+sname(sname.First('_')+1,10)+"_";		  
	
}


double resonance_mass( double em_upper){ // em en mev
   
	// delta p e pp 1869.4
	// delta 0  1.23
	double width = 1.18 * 100.;  // GeV -> MeV
	double em = 1.232 * 1000;
	double lower_cut = em - ( width + 10. );  
	double m_rss = 0.;
  
	if ( em_upper >= lower_cut ) { 
		double alpha = atan( 2. * ( lower_cut - em ) / width );  
		double beta  = atan( 2. * ( em_upper - em ) / width );    
		double psi = gRandom->Uniform();
		
		m_rss = em + .5 * width * tan( alpha + psi * ( beta - alpha ) );    
    
		if ( m_rss > em_upper ) 
			m_rss = em_upper;
		if ( m_rss < lower_cut )
			m_rss = lower_cut;    
	}
	else{ 
		//std::cout << "Warning" << std::endl;
		return em_upper;
	}  
	return m_rss;  
}

//____________________________________________________________________________________________										
double breitWigner( Double_t sqrt_s, Double_t* p ){
	Double_t mass_adjust = p[0];
	Double_t Gamma_0 = p[1];  
	Double_t sigma0 = p[2];
	Double_t d = ( TMath::Power( TMath::Power(sqrt_s, 2) - TMath::Power(mass_adjust,2), 2) + TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2) );
	return sigma0 * (TMath::Power(mass_adjust,2) * TMath::Power(Gamma_0,2))/d; 
}



//____________________________________________________________________________________________										

void plotdata(){
    
  TMultiGraph* mg[8]; // 
  
    for(int i = 0; i < 8; i++){
      mg[i] = new  TMultiGraph();
    }
    
  const int dzise = 18; 
  TGraphErrors edata[dzise];
 
  int counter = 0;
  int counter2 = 0;
  
  TString basecc  = "CCqe%de";
  TString ccname;
  
  bool deu = true; 
  bool carb = false;  
  bool Al =  false;  
  bool Nomad =  false; 
  bool cern =  false; 

  
  //  cout << ccname << endl;
  
  for(int i = 1; i < 6; i++) {
    ccname= Form(basecc.Data(), i);
//    cout << ccname << endl;
    edata[counter] = TGraphErrors("data/"+ccname+".txt","%lg %lg %lg");
    mg[counter2]->SetTitle("#nu + n #rightarrow #mu^{-} + p ");
    
    if(deu && i==1)
      mg[counter2]->Add(&edata[counter]);

    if(carb && i>1 && i<4 )
      mg[counter2]->Add(&edata[counter]);
    
    if(Al && i==4)
      mg[counter2]->Add(&edata[counter]);

    if(Nomad && i==5)
      mg[counter2]->Add(&edata[counter]);
    
        counter++;
  }
  
  counter2++;
  
  TString baseres  = "CCres%de";
  TString resname;

  
  for(int i = 1; i < 8; i++) {
    resname= Form(baseres.Data(), i);
 //   cout << resname << endl;
    edata[counter] = TGraphErrors("data/"+resname+".txt","%lg %lg %lg");
        
  
      if(i<3){
	    mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{++} ");
	if(deu && i==2){
	  counter++;
	  continue;
	}
//      mg[counter2]->Add(&edata[counter]);
      } // end res1 
      
	  
      if(i==4){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi{+} + n "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==5 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
      }
      
        if(i==6){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi{0} + p "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==7 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
  
      }
      
    cout << counter2 << " c2 " << endl;  
    mg[counter2]->Add(&edata[counter]);	  
    counter++;  
  }

  
  counter2++;
  
  TString Nbaseres  = "NCres%de";
  TString Nresname;

  TString name[4] = {"#nu+p #rightarrow #nu + #Delta^{+} decay in #pi^{+} + n ","#nu+p #rightarrow #nu + #Delta^{+} decay in #pi{0} + p ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{0} + n ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{-} + p "};
  
     
  Nresname= Form(Nbaseres.Data(), 1);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[0]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;

  
  Nresname= Form(Nbaseres.Data(), 2);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 3);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;

  Nresname= Form(Nbaseres.Data(), 4);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[2]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  
  Nresname= Form(Nbaseres.Data(), 5);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 6);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
 // counter2++;
  
  
  cout << counter << endl;
  cout << counter2 << endl;

 // /home/jlbernal/cross/data/NCres1e.txt

  
      TString pbase  = "res/test%de.pdf";
      TString npr;
      
        

      
      for(int i = 0; i < 8; i++){
        npr= Form(pbase.Data(), i);
	cout << npr << endl;
       	TCanvas *c1 = new TCanvas("c1","",800,600) ;
	mg[i]->Draw("AP");
	c1->Print(npr);
	delete c1;
    } 
  
 
   /* TCanvas *c1 = new TCanvas("c1","",800,600) ;
    mg[1]->Draw("AP");
    //c1->SetLogx();  
    c1->Print("other.pdf");
    delete c1;
*/
 
 
     for(int i = 0; i < 8; i++){
      delete mg[i]; 
    }
   
  
  
  cout << "test" << endl;
  
  
}
