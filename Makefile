CPP         = g++
LINKER      = g++
CPPSRCPOST  = cc
CPPHDRPOST  = h
OBJPOST     = o
EXEPOST     = exec
SOPOST      = so

ROOTPATH    = $(ROOTSYS)
IPATH       = $(shell root-config --libs) -I.

CFLAGSDBG   = $(shell root-config --cflags) -ggdb3 -D DEBUG -Wall -D MPICH_SKIP_MPICXX -D OMPI_SKIP_MPICXX -fexceptions -fPIC -D TIXML_USE_STL
CFLAGS      = $(CFLAGSDBG)


CNAME       = cross
COBJECT     = $(CNAME).$(EXEPOST)
CLINKOBJ     = -o $(COBJECT)
CPPSRCFILES = cross.cc
CPPHEADER =	$(CPPSRCFILES:.cc=.h)


$(CNAME): 
	 $(CPP) $(CLINKOBJ)  $(CPPSRCFILES) $(CPPHEADER)  $(CFLAGSDBG) $(IPATH) -std=c++0x

clean:
	 rm *.exec





